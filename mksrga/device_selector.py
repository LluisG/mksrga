import sys
import tango

from PyQt5.QtGui import QIcon
from taurus.qt.qtgui.input.tauruscombobox import TaurusAttrListComboBox
from taurus.qt.qtgui.base import TaurusBaseWidget

import taurus
from PyQt5 import Qt, uic

"""Code to work with Qt widgets"""

# class GetDevice(Qt.QWidget, TaurusBaseWidget):
class GetDevice(Qt.QDialog):

    def __init__(self, parent=None):
        Qt.QWidget.__init__(self, parent=parent)
        # TaurusBaseWidget.__init__(self, name='GetDevice')
        # app = Qt.QApplication(sys.argv)
        #TaurusAttrListComboBox.__init__(self)

        self.setWindowIcon(QIcon('logoRGAGUI.png'))


        self.device  = ''
        self.lay = Qt.QVBoxLayout(self)
        self.deviceList = TaurusAttrListComboBox()
        self.deviceList.addItems(self.show_dev())

        self.button = Qt.QPushButton('Select Device')
        self.button.clicked.connect(self.selectRGA)
        # self.button.clicked.connect(lambda: self.close())

        self.lay.addWidget(self.deviceList)
        self.lay.addWidget(self.button)

        self.selected = False

        self.show()
        # sys.exit(app.exec())
    def show_dev(self):
        dev_type = 'mksRGA'
        tg_db = tango.Database() #taurus.Authority('tango://localhost:10000')
        devnames = []
        for name in tg_db.get_instance_name_list(dev_type):
            server_name = '/'.join((dev_type, name))
            for devname in tg_db.get_device_class_list(server_name)[::2]:
                if not devname.lower().startswith('dserver/'):
                    devnames.append(devname)
                    # print(devnames)
        return devnames

    def selectRGA(self):
        myRga = self.deviceList.currentText()
        """for testing TO BE REMOVED"""
        # myRga = " test/tango_layer/1"
        # user_gui.myRGA = myRga
        # print( 'RGA choosen: ', myRga)

        self.close()

        return myRga



        # def changeDevice(self):
        #     return (self.currentText())


if __name__ == '__main__':
    app = Qt.QApplication(sys.argv)

    # instantiate the widget
    w = GetDevice()
    w.setWindowTitle("Select Device")


    # show it (if you do not show the widget, it won't be visible)
    w.show()

    # Initialize the Qt event loop (and exit when we close the app)
    sys.exit(app.exec_())
