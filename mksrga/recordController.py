
import tango
import numpy as np
import datetime
import threading

from PyQt5.QtCore import QObject
from PyQt5.QtCore import QObject, QThread, pyqtSignal

class RecordController(QObject):
    recorded = pyqtSignal(str)
    def __init__(self, model, mode, fileName):
        super().__init__()
        self.rga=tango.DeviceProxy(model)
        self.fileName=fileName
        self.mode=mode
        self.stopRecord = threading.Event()





    def startRecord(self):

        with open(self.fileName, "w+") as file:
            timeStamp = datetime.datetime.now()
            timeStamp = timeStamp.strftime("%x, %X")
            file.write(str(timeStamp + "," + "\n"))
            # file.write(self.rga.read_attribute("GeneralInfo").value)
            # print(self.rga.read_attribute("GeneralInfo").value)
            # file.write("\n")
            if self.mode == "analog":
                masses = self.rga.read_attribute("AnalogMasses").value
                self.pressuresAttribute="AnalogPressures"
            elif self.mode == "peak":
                masses = self.rga.read_attribute("PeakMasses").value
                self.pressuresAttribute="PeakPressures"
            # print(masses)
            for i in masses:
                file.write("," + str(i))
            file.write("\n")
            file.close()



        self.th = threading.Thread(target=self.recordScan())

        self.th.start()
        print("OKOK44")

    def recordScan(self):
        print("OKOK")

        self.b = self.rga.read_attribute(self.pressuresAttribute).value
        while not self.stopRecord.isSet():

            self.pressure = self.rga.read_attribute(self.pressuresAttribute).value
            if not np.array_equal(self.b[0], self.pressure[0]):
                print(self.pressure)
                self.b = self.pressure
                self.pressure=np.delete(self.pressure,0)
                with open(self.fileName, "a") as file:
                    file.write(datetime.datetime.now().strftime("%X"))
                    for i in self.pressure:
                        file.write("," + str(i))
                    file.write("\n")
                    file.close()
                    strpp=str(self.pressure)
                    print(strpp)
                    self.recorded.emit(strpp)
        print("stop recording")

    def recordComment(self,comment):
        print(comment)
        with open(self.fileName, "a") as file:
            file.write("##### " + comment)
            file.write("\n")
            file.close()


if __name__ == "__main__":

      v=RecordController('comedor/mksRGA/1')
