import os
import sys
import time

from PyQt5.QtCore import Qt
from PyQt5 import Qt, QtCore
from PyQt5.QtGui import QIntValidator
from taurus.qt.qtgui.base import TaurusBaseWidget
from taurus.qt.qtgui.application import TaurusApplication
from taurus.qt.qtgui.input import TaurusValueLineEdit
from taurus.qt.qtgui.base import TaurusBaseComponent

from taurus.qt.qtgui.application import TaurusApplication
from taurus.qt.qtgui.display import TaurusLabel

from PyQt5.QtWidgets import QSlider, QWidget, QVBoxLayout, QLineEdit
from taurus.core.taurusattribute import TaurusAttribute
import tango


class GenericSliderLine(Qt.QWidget):

    def __init__(self, model, attr, min, max, step, parent=None):
        # super().__init__(parent)
        QWidget.__init__(self, parent=parent)
        # app = TaurusApplication(sys.argv, cmd_line_parser=None, )

        self.rga = tango.DeviceProxy(model)
        self.attr = attr
        self._last = time.time()
        self.step = step
        self.now = time.time()

        self.layout = QVBoxLayout()
        self.setLayout(self.layout)

        self.nameLabel = TaurusLabel()

        self.nameLabel.model, self.nameLabel.bgRole = model + "/" + attr + '#label', ''
        self.nameLabel.setAlignment(QtCore.Qt.AlignLeft)

        self.input = Qt.QLineEdit()
        self.input.setText(str(self.rga.read_attribute(attr).value))

        self.input.setAlignment(QtCore.Qt.AlignCenter)

        self.slider = Qt.QSlider()
        self.slider.setOrientation(QtCore.Qt.Horizontal)
        self.slider.setMinimum(min)
        maximum = (max * (1 / step))

        self.slider.setMaximum(maximum)
        # self.slider.setSingleStep()

        self.layout.addWidget(self.nameLabel)
        self.layout.addWidget(self.input)
        self.layout.addWidget(self.slider)

        # self.qLine.setModel(model)

        self.input.editingFinished.connect(self.updateSlider)
        # self.slider.setValue(self.qLine.text())
        self.slider.sliderReleased.connect(self.writeAttr)
        self.slider.sliderMoved.connect(self.updateLabel)

        self.updateSlider()
        self.show()
        # sys.exit(app.exec_())


    def updateLabel(self):
        text = '%1.2f' % (self.slider.value()) * self.step

        self.input.setText(str(text))

        # self.writeAttr(float(text))

        # self.qLine.setAttribute(self.slider.value())

    def updateSlider(self):

        value = float(self.input.text())
        # value=value.split(" ")
        # print(value)
        # print(value[0], type(value[0]))
        # value=float(value[0])

        valueToSlider = value * (1 / self.step)

        self.slider.setValue(valueToSlider)

        self.writeAttr()

    def writeAttr(self):
        value = float(self.input.text())

        self.now = time.time()
        if self.now - self._last > 0.05:

            self.rga.write_attribute(self.attr, value)

            # self.rga.write_attribute(self.attr, float(self.slider.value()))
            self._last = self.now
        else:
            pass


if __name__ == "__main__":
    app = TaurusApplication()
    model = 'comedor/mksRGA/1'
    w = GenericSliderLine(model, 'ExtractorVoltage', -1300, 0, 0.1)
    w.show()
    sys.exit(app.exec_())
