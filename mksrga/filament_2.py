import os
import sys
from taurus.qt.qtgui.base import TaurusBaseWidget
from PyQt5 import Qt, uic
from mksrga.degas import DegasManager
from PyQt5.QtWidgets import QPushButton
from taurus.qt.qtgui.display import TaurusLed
from taurus.qt.qtgui.button import TaurusCommandButton
import tango

class FilamentManager(Qt.QWidget, TaurusBaseWidget):
    def __init__(self, model,parent=None):
        Qt.QWidget.__init__(self , parent=parent)
        TaurusBaseWidget.__init__(self, name='FilamentManager')

        self.rga = tango.DeviceProxy(model)
        self.model=model
        self.turnOn = True

        mainlay=Qt.QVBoxLayout()
        radiolay=Qt.QHBoxLayout()
        buttonlay=Qt.QHBoxLayout()

        self.radio1=Qt.QRadioButton("Fil 1")
        self.radio2=Qt.QRadioButton("Fil 2")

        a = self.rga.read_attribute("filamentSelected").value
        if a == 1:
            self.radio1.setChecked(True)
        else:
            self.radio2.setChecked(True)

        self.led=TaurusLed()
        self.led.setModel(model + "/State")
        self.led.setFixedWidth(20)


        self.buttonRun=TaurusCommandButton(command='FIL_ON')
        self.buttonRun.clicked.connect(self.filFunc)
        self.buttonRun.setModel(model)
        self.buttonRun.setText("ON")

        self.degassButton=Qt.QPushButton()
        self.degassButton.clicked.connect(self.degas)
        self.degassButton.setText("Degas")


        self.radio1.toggled.connect(self.selec1)
        self.radio2.toggled.connect(self.selec2)

        radiolay.addWidget(self.radio1)
        radiolay.addWidget(self.radio2)

        buttonlay.addWidget(self.buttonRun)
        buttonlay.addWidget(self.led)

        mainlay.addLayout(radiolay)
        mainlay.addLayout(buttonlay)

        mainlay.addWidget(self.degassButton)
        self.setLayout(mainlay)

        # self.show()

    def selec1(self):
        self.rga.command_inout("SelectFil1")
    def selec2(self):
        self.rga.command_inout("SelectFil2")

    def filFunc(self):
        if self.turnOn:
            self.buttonRun.setCommand("FIL_OFF")
            self.buttonRun.setText("OFF")
            self.turnOn=False
        else:
            self.buttonRun.setCommand("FIL_ON")
            self.buttonRun.setText("ON")
            self.turnOn=True

    def degas(self):
        self.degas=DegasManager(self.model)
        self.degas.show()

if __name__ == "__main__":
    # Initialize a Qt application (Qt will crash if you do not do this first)
    app = Qt.QApplication(sys.argv)

    # instantiate the widget
    w = FilamentManager("comedor/mksRGA/1")
    #w.setModel('lab/vc/rga-01')
    w.setWindowTitle("Filament")

    # show it (if you do not show the widget, it won't be visible)
    w.show()

    # Initialize the Qt event loop (and exit when we close the app)
    sys.exit(app.exec_())
