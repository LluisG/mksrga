import sys
# from PyQt5 import Qt

"""esta es la buena"""
from taurus.external.qt import Qt
import PyQt5.QtWidgets
# from taurus.qt.qtgui.base import TaurusBaseWidget
from taurus.qt.qtgui.input.tauruscombobox import TaurusAttrListComboBox
from taurus.qt.qtgui.button.taurusbutton import TaurusCommandButton

import tango

class AddMassWidget(Qt.QWidget):
    def __init__(self, model, widgetNumber, parent=None):
        # TaurusWidget.__init__(self, parent=parent)
        Qt.QWidget.__init__(self, parent=parent)
        # TaurusBaseWidget.__init__(self, name="AddMass")

        # super().__init__(parent)
        self.widgetNumber = widgetNumber

        self.ds=tango.DeviceProxy(model)

        self.nScan = Qt.QLabel()
        self.nScan.setText(str(self.widgetNumber))
        self.addMass=False
        self.mass = 1
        self.sensor = 0
        self.gain = 0
        self.accuracy = 0

        # self.panel = Qt.QWidget()
        self.layout = Qt.QHBoxLayout(self)
        # self.panel.setLayout(self.layout)


        """
        "Structure of add mass: mass, detector, accuracy, gain"
        """

        self.massBox = Qt.QLineEdit()
        self.accuracyCombo = Qt.QComboBox()
        self.sensorsBox = TaurusAttrListComboBox()
        self.gainBox = TaurusAttrListComboBox()

        self.button = TaurusCommandButton(command='PeakScanAddMass',
                                          parameters=[self.mass,self.accuracy,
                                                      self.gain,self.sensor])




        self.button.setModel(model)
        self.button.setText("AddMass")

        self.massBox.setValidator(Qt.QIntValidator(1,100))
        self.massBox.setMaxLength(2)
        self.massBox.setText("")
        self.massBox.setToolTip("Mass to scan")
        # self.massBox.editingFinished.connect(self.massChanged)

        for i in range(9):
            self.accuracyCombo.addItem(str(i))
        # self.accuracyCombo.currentIndexChanged.connect(self.accuracyChanged)



        self.sensorsBox.setModel(model + '/Sensors')
        # print("MODEL widget", self.sensorsBox.getFullModelName())

        self.sensorsBox.setToolTip("Sensor to use for Scan")
        # self.sensorsBox.currentIndexChanged.connect(self.sensorChanged)

        """gainBox"""
        self.gainBox.setToolTip("Gain to be used")
        self.gainBox.setModel(model + '/Gains')
        self.gainBox.currentIndexChanged.connect(self.gainChanged)

        # self.button.setModel=model
        # self.button.setModel='comedor/mksRGA/1'
        self.button.setToolTip("Add this set to the scan")
        self.button.setText("Add Mass")
        self.button.clicked.connect(self.setButtonCommand)

        self.layout.addWidget(self.nScan)
        self.layout.addWidget(self.massBox)
        self.layout.addWidget(self.sensorsBox)
        self.layout.addWidget(self.gainBox)
        self.layout.addWidget(self.accuracyCombo)
        self.layout.addWidget(self.button)

        # self.panel.show()


    def massChanged(self):
        self.mass=int(self.massBox.text())
        # self.mass = int(text)
        self.setParams()
        print (self.mass)

    def sensorChanged(self):
        self.sensor=self.sensorsBox.currentIndex()
        self.setParams()
        print(self.sensor)

    def gainChanged(self):
        self.gain=self.gainBox.currentIndex()
        self.ds.write_attribute("GainSlected",self.gainBox.currentIndex() )


        self.setParams()
        print(self.gain)

    def accuracyChanged(self):
        self.accuracy=self.accuracyCombo.currentIndex()
        self.setParams()
        print(self.accuracy)

    def setParams(self):
        self.button.setParameters([self.mass,self.accuracy,self.gain, self.sensor])
        print(self.button.getParameters())

    def setButtonCommand(self):
        print(self.mass,self.sensor,self.accuracy,self.gain)
        if self.addMass:
            self.button.setCommand("PeakScanAddMass")
            self.setParams()
            # self.mass = self.massBox.text()
            # self.accuracy=self.accuracyCombo.currentIndex()
            # self.gain=self.gainBox.currentIndex()
            # self.sensor=self.sensorsBox.currentIndex()
            self.button.setParameters=([self.mass,self.accuracy,self.gain,self.sensor])

            self.button.setText("AddMass")
            self.addMass=False
            self.enableWidgets(False)
        else:
            self.button.setCommand("PeakScanRemoveMassWeight")
            self.button.setParameters([self.mass])
            self.button.setText("Remove Mass")
            self.addMass=True
            self.enableWidgets(True)





    def enableWidgets(self, bool):

        self.sensorsBox.setDisabled(bool)
        self.accuracyCombo.setDisabled(bool)
        self.massBox.setDisabled(bool)
        self.accuracyCombo.setDisabled(bool)
        self.gainBox.setDisabled(bool)

    # def setModelWidget(self, model):
    #     super(AddMassWidget, self).setModel(model)
    #     print("MODEL SETTED",model)
    #     self.button.setModel(model)
    #     self.gainBox.setModel(model+"/Gains")
    #     self.sensorsBox.setModel(model+"/Sensors")

    # def setParams(self):
    #     self.accuracy=self.accuracyCombo.currentIndex()
    #     self.gain=self.gainBox.currentIndex()
    #     self.sensor=self.sensorsBox.currentIndex()
    #     self.mass=self.massBox.text()

if __name__ == "__main__":
    # Initialize a Qt application (Qt will crash if you do not do this first)
    app = Qt.QApplication(sys.argv)

    # instantiate the widget
    f = AddMassWidget("comedor/mksRGA/1", 0)
    # f.setWindowTitle("Masses to scan")
    f.show()
    sys.exit(app.exec_())

