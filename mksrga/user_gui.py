import sys

from PyQt5 import Qt
from taurus.qt.qtgui.application import TaurusApplication
from taurus.qt.qtgui.taurusgui import TaurusGui

from taurus.qt.qtgui.panel import TaurusDevicePanel

from device_selector import GetDevice as getDev
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QApplication, QWidget


from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from diagnostics import RunDiagnostic as rd
from genericSliderLine import GenericSliderLine
from parameters import Parameters as param
from peakScan import PeakScanWidget
from analog_barScan import AnalogScanForm
import tango


class MKSRGA(QMainWindow):

    def __init__(self):
        super().__init__()

        # self.initUI()

        # def initUI(self):
        self.getd = getDev()

        self.getd.show()
        self.getd.exec_()
        self.model = self.getd.selectRGA()
        self.rga = tango.DeviceProxy(self.model)

        print(self.model)
        print(type(self.model))

        try:
            self.rga.read_attribute("State")
        except:
            self.setDisabled(True)
            self.errorPoPuP()


        self.setWindowIcon(QIcon('logoRGAGUI.png'))
        self.setWindowTitle(self.model)



        """Info menu"""
        menubar = self.menuBar()

        info = menubar.addMenu("Info")
        genInfo = QAction("General Info", self)
        info.addAction(genInfo)
        diag = QAction("Run Diagnostic", self)
        info.addAction(diag)

        diag.triggered.connect(self.getDiag)
        info.triggered.connect(self.getInfo)

        config = menubar.addMenu("Config")
        setConfig = QAction("Set Parmeters", self)
        config.addAction(setConfig)
        setConfig.triggered.connect(self.configWindow)

        self.statusBar()

        # self.setGeometry(500, 500, 500, 500)


        self.scanWidget = QWidget()
        self.scanLay = QVBoxLayout()
        self.comboScan = QComboBox()
        self.comboScan.addItems([ "Analog Scan","Peak Scan"])
        self.comboScan.activated.connect(self.switchScan)
        self.stackedLayout = QStackedLayout()

        self.analogScan = AnalogScanForm(self.model)
        self.peakScan = PeakScanWidget(self.model,rga=self.model)
        self.stackedLayout.addWidget(self.analogScan)
        self.stackedLayout.addWidget(self.peakScan)

        self.scanWidget.setLayout(self.scanLay)
        self.scanLay.addWidget(self.comboScan)
        self.scanLay.addLayout(self.stackedLayout)




        self.setCentralWidget(self.scanWidget)
        self.show()

    def getDiag(self):
        rd(self.model)
        # showDiag.show()

    def getInfo(self):
        print("fine!")
        popup = QMessageBox()
        popup.setWindowTitle("Info")
        text = self.rga.read_attribute("GeneralInfo")
        popup.setText(text.value)
        popup.setIcon(QMessageBox.Information)
        popup.setStandardButtons(QMessageBox.Close)
        popup.exec_()

    def configWindow(self):
        print("fine!")
        # self.line=GenericSliderLine(self.model, 'ExtractorVoltage', -1300, 0, 0.1)
        param(self.model)
        param.show(self)


    """SCAN type selector"""

    def switchScan(self):
        self.rga.command_inout("StopScan")
        self.stackedLayout.setCurrentIndex(self.comboScan.currentIndex())

    def errorPoPuP(self):

        text = "There is no communication with the device."

        popup = QMessageBox()
        popup.setWindowTitle("ERROR")
        popup.setText(text)
        popup.setIcon(QMessageBox.Critical)
        popup.setStandardButtons(QMessageBox.Close)
        popup.exec_()


def main():
    app = QApplication(sys.argv)
    w = MKSRGA()
    w.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
