import sys
from PyQt5 import Qt
from PyQt5 import Qt, QtCore
from PyQt5.QtGui import QIcon
from taurus.qt.qtgui.base import TaurusBaseWidget
from genericSliderLine import GenericSliderLine as gnsl
from taurus.qt.qtgui.application import TaurusApplication


class Parameters(Qt.QWidget):

    def __init__(self, model):
        Qt.QWidget.__init__(self, parent=None)
        # app2 = Qt.QApplication(sys.argv)

        self.setWindowIcon(QIcon('logoRGAGUI.png'))

        self.mainLayout=Qt.QHBoxLayout()
        self.v_layout1=Qt.QVBoxLayout()
        self.v_layout2=Qt.QVBoxLayout()

        HMAattr="HighMassAlignment"
        self.HMA=gnsl(model,HMAattr,1,65535,1)
        self.v_layout1.addWidget(self.HMA)
        self.HMA.setToolTip("Unitless")

        LMAattr="LowMassAlignment"
        self.LMA=gnsl(model,LMAattr,1, 65535,1)
        self.v_layout1.addWidget(self.LMA)
        self.LMA.setToolTip("Unitless")

        HMRattr="HighMassResolution"
        self.HMR=gnsl(model,HMRattr,1, 65535,1)
        self.v_layout1.addWidget(self.HMR)
        self.HMR.setToolTip("Unitless")

        LMRattr= "LowMassResolution"
        self.LMR=gnsl(model, LMRattr,1, 65535,1)
        self.v_layout1.addWidget(self.LMR)
        self.LMR.setToolTip("Unitless")

        EEattr= "ElectronEnergy"
        self.EEn=gnsl(model,EEattr,0,100,0.1)
        self.v_layout2.addWidget(self.EEn)
        self.EEn.setToolTip("Units: eV")

        Ecurrentattr="ElectronEmission"
        self.EEm=gnsl(model, Ecurrentattr,0,5,0.01)
        self.v_layout2.addWidget(self.EEm)
        self.EEm.setToolTip("Units: mA")

        IEattr="IonEnergy"
        self.IE=gnsl(model, IEattr,0, 10,0.1)
        self.v_layout2.addWidget(self.IE)
        self.IE.setToolTip("Units: eV")


        EVattr="ExtractorVoltage"
        """minimun value in device is -130. -1300 due to way of working method"""
        self.EV=gnsl(model, EVattr,-1300, 0,0.1)
        self.v_layout2.addWidget(self.EV)
        self.EV.setToolTip("Units: V")


        self.setLayout(self.mainLayout)
        self.mainLayout.addLayout(self.v_layout1)
        self.mainLayout.addLayout(self.v_layout2)

        self.show()
        print("show")
        # self.exec_()
        sys.exit(app.exec_())

if __name__ == "__main__":
    app2 = Qt.QApplication(sys.argv)
    w = Parameters('comedor/mksRGA/1')

    w.show()
    sys.exit(app2.exec_())
